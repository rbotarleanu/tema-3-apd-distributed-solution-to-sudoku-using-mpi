/* 
 *	Botarleanu Robert-Mihai 331CB.
 *  Source file, please see the header for function descriptions.
 */


#include "debugging.h"

void print_vec(int *v, int size) { 
	int i = 0;
	for (; i < size; ++i) fprintf(LOG_FILE, "%d ", v[i]);
	fprintf(LOG_FILE, "\n");
}

void print_mat(unsigned char *A, int height, int width) { 
	int i = 0, j = 0;
	for (; i < height; ++i) {
		for(j = 0; j < width; ++j)
		 fprintf(LOG_FILE, "%d ", A[i * height + j]);
		fprintf(LOG_FILE, "\n");
	}
	fprintf(LOG_FILE, "\n");
}

const char* conv_tag(int tag) {
	switch(tag) {
		case REQUEST: return "SONDAJ";
		case ECHO: return "ECOU";
		case ECHO_FALSE: return "ECOU_FALS";
		case TOP: return "TOP";
	}	
}
