/* 
 *	Botarleanu Robert-Mihai 331CB.
 *  Source file, please see the header for function descriptions.
 */


#include "list.h"

vec_cpy(int *dest, int *src, int size) {
	int	i;
	// copy each element
	for(i = 0; i < size; ++i) dest[i] = src[i];
}

void cell_destroy(list *cell) {
	// deallocate memory for data and cell
	free((*cell)->a);
	free(*cell);
}

void list_destroy(list *l) {
	if(!l) return;
	// go through each cell
	while(*l != NULL) {
		list dstr = *l;
		*l = (*l)->next;
		cell_destroy(&dstr);	// destroy each cell
	}
}

list list_make_cell(int *el, int size) {
	int 		i;
	// allocate memory for cell
	list cell = (list) calloc(1, sizeof(node));
	cell->next = NULL;	// size 1 list
	if(cell) {
		// put the data
		cell->size = size;	
		cell->a = (int*) calloc(size, sizeof(int));
		vec_cpy(cell->a, el, size);
	}
	return cell;
}

void list_add(list *l, int pos, int *el, int size) {
	int i = 0;
	list cell = list_make_cell(el, size);
	// add to empty list
	if(!*l) {
		*l = cell;
		return;
	}
	// add to the beginning
	if(pos == 0) {
		cell->next = *l;
		*l = cell;
		return;
	}
	// find the pointer to the previous cell in the list
	list aux = *l;
	for(; aux != NULL && i < pos - 1; ++i) aux = aux->next;
	if(!aux) return;
	cell->next = aux->next;
	aux->next = cell;
}

list list_remove(list *l, int pos) {
	if(!l) return;
	int i;
	// remove from the first position: redirect pointer
	if(pos == 0) {
		list cell = *l;
		*l = (*l)->next;
		cell_destroy(&cell);
		return;
	}
	// find the pointer to the previous cell in the list
	list aux = *l;
	for(i = 0; aux != NULL && i < pos - 1; ++i) aux = aux->next;
	if(!aux) return;
	list cell = aux->next;
	aux->next = cell->next;
	cell_destroy(&cell);
}

void list_print(list l) {
	if(!l) return;
	// print the list, cell by cell
	list aux;
	int i, cnt = 0;
	for(aux = l; aux != NULL; aux = aux->next, ++cnt) {
		fprintf(LOG_FILE, "Cell %d: ", cnt);
		for( i = 0; i < aux->size; ++i) fprintf(LOG_FILE, "%d ", aux->a[i]);
		fprintf(LOG_FILE, "\n");
	}
}

int list_size(list l) {
	if(!l) return 0;
	list aux = l;
	int size = 0;
	while(aux != NULL) { 
		size += aux->size;  
		aux = aux->next; 
	}
	return size;
}

int* compress_to_array(list l, int *len) {
	if(!l) return NULL;
	*len = list_size(l);	// find total number elements to compress
	int i = 0, j;
	// allocate the memory for the array
	int *v = (int*) calloc(*len, sizeof(int));
	list aux = l;
	while(aux != NULL) {
		// put the elements, one by one in the compressed array
		for(j = 0; j < aux->size; ++j)
			v[i++] = aux->a[j];
		aux = aux->next;
	}
	return v;	// return the compressed array
}

list decompress_from_array(int* v, int len, int tsize) {
	list l = NULL;
	printf("Len = %d  tsize = %d\n", len, tsize);
	int i = 0;
	while(i < len) {
		// put the current block as a cell in the list
		int *temp = (int *) calloc(tsize, sizeof(int));
		vec_cpy(temp, &v[i], tsize);
		list_add(&l, 0, temp, tsize);
		free(temp);
		i += tsize;	// go to next block
	}
	return l;	// return the compressed list
}
