/* 
 *	Botarleanu Robert-Mihai 331CB.
 *  The topology-wave algorithm.
 */


#ifndef TOPOLOGY_HEADER_GUARD
#define TOPOLOGY_HEADER_GUARD

// tags
enum {REQUEST, ECHO, ECHO_FALSE, TOP};

#include "mpi.h"
#include "main.h"

// constructs the topology by using wave messages
void construct_topology(void);

#endif