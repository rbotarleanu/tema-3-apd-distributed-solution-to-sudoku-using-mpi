/*
 *	Botarleanu Robert-Mihai 331CB
 *  Global variables and constants.
 */

#ifndef MAIN_HEADER_GUARD
#define MAIN_HEADER_GUARD

typedef unsigned char byte;
typedef char bool;

enum {false, true};

#define min(a,b) ((a)<(b))?(a):(b)
#define max(a,b) ((a)>(b))?(a):(b)

// tags for the sudoku solving algorithm used for 
// MPI messages
enum{ PROBLEM, PROBLEM_ARRAY, UPDATE, SUDOKU_DONE };

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "topology.h"
#include "mpi.h"
#include "debugging.h"
#include "sudoku.h"
#include "list.h"

#define ROOT					0
#define MAX_FILE_NAME_LEN		255	 
#define MAX_LINE_SIZE			1024
#define MAX_TOPOLOGY_SIZE		16
#define DEFAULT_MAX_SOLUTIONS   100000
#define MAX_BUFFER_SIZE			99000000

char 		TOPOLOGY_FILE[MAX_FILE_NAME_LEN];
char		IN[MAX_FILE_NAME_LEN];
char 		OUT[MAX_FILE_NAME_LEN];
FILE*		LOG_FILE;
FILE*		outf;

int			rank;

int 		N;
int	   		adj[MAX_TOPOLOGY_SIZE];
int 		adj_size;
byte		*A;
byte		*buf;
int			*routing_table;

int 		adj_size;
int			parent;

bool		false_parents[MAX_TOPOLOGY_SIZE];

int			*sudoku;
int			pb_size;
int			max_solutions;

bool		solution_found;

MPI_Status 	status;	

int logpow(int base, int exponent);	

#endif