Botarleanu Robert-Mihai 331CB - Tema 3
--------------------------------------------------------------------------------
	Cuprins
		1. Descrierea problemei
		2. Descrierea ierarhiei programului
		3. Descrierea implementarii algoritmului de stabilire a topologiei cu
		mesaje de tip unda
			i)  Stabilirea topologiei
			ii) Constructia tabelei de rutare
		4. Descrierea implementarii algoritmului de rezolvare Sudoku
			i)  Etapa secventiala
			ii) Comunicarea intre procese
		5. Probleme intampinate si solutii gasite
		6. Compilare
--------------------------------------------------------------------------------

	1. Descrierea problemei
	In scopul realizarii temei, am implementat un algoritm de rezolvare a 
unui joc Sudoku de dimensiune variabila folosind libraria MPI pentru comunicarea
intre procese.
	Tiparele de programare principale folosite sunt algoritmii de tip unda si 
algoritm de tip Backtracking pentru a genera toate posibilitatile de a rezolva
un patrat dintr-un joc Sudoku.

	2. Descrierea ierarhiei programului
	Programul este organizat in mai multe fisiere, fiecare rezolvand un anumit
aspect al problemei:
	- main.h/main.c: contin implementarea propriu-zisa a algoritmului descris 
	in enuntul problemei pentru a rezolva folosind procese si comunicare prin 
	intermediul MPI a unui joc sudoku
	- list.h/list.c: contin implementarea unei liste simplu inlantuite, cu 
	date de tip array unidimensional
	- topology.h/topology.c: contin implementarea algoritmului de stabilire
	a topologiei cu mesaje de tip unda
	- sudoku.c/sudoku.h: contin algoritmi secventiali de generare a solutiilor
	unei subprobleme dintr-un joc de Sudoku, precum si combinarea unei solutii
	cu alte solutii de la alte subpatrate.

	3. Descrierea implementarii algoritmului de stabilire a topologiei cu
	mesaje de tip unda
	i) Stabilirea topologiei
	Algoritmul de stabilire a topologiei cu mesaje de tip unda este bazat pe 
cel facut in cadrul laboratorului 10. 
	Algoritmul porneste prin citirea unor liste de adiacenta de catre fiecare
nod/proces dintr-un fisier dat ca argument din linia de comanda. 
	Procesul Master/Root - ales ca fiind procesul cu rang 0 - va initia
algorimtul prin trimiterea unor cereri de tip REQUEST catre copii sai directi, 
acestia vor retine faptul ca procesul de la care au primit REQUEST-ul este 
"parintele" lor si vor propaga mesajul mai departe in topologie.
	Primul mesaj de tip REQUEST primit de catre un proces va desemna parintele 
acelui proces, in cazul in care ulterior sunt primite alte mesaje, emitatorii 
acestor mesaje vor fi marcati ca fiind "false_parents" iar cererile lor vor 
fi raspunse cu ECHO_FALSE. Pe de alta parte, parintii "reali" vor primi mesaje
de tip ECHO.
	Prin aceasta secventa de trimitere de mesaje REQUEST si primire de mesaj
ECHO, impreuna cu trimiterea topologiei cunoscute de fiecare proces, procesul
Master/Root va putea reconstrui topologia completa, difuzand-o inapoi prin 
mesaje de tip TOP pe ierarhie.

	ii) Constructia tabelei de rutare
	In timpul rularii algoritmului descris mai sus, se poate ca fiecare proces
sa isi construiasca tabela de rutare proprie astfel:
	- pentru orice mesaj de tip ECHO primit de la unul dintre copiii sai, 
	se vor putea introduce intrari in tabela de rutare cu next-hop luat ca 
	fiind copilul care a trimis mesajul. 
	- la final, dupa ce algoritmul de stabilire a topologiei s-a finalizat, 
	fiecare	nod va completa tabela sa de rutare prin setarea next-hopurilor 
	pentru toate coloanele goale din tabela ca fiind parintele procesului. 
	Acest lucru este posibil deoarece, daca un nod nu poate la un alt nod 
	mergand in jos prin ierarhie(nu ii este parinte direct, sau indirect) 
	atunci se va putea ajunge cu siguranta la acel nod mergand prin parinte(
	si eventual, traversand prin parinti pana se ajunge la nodul radacina).

	4. Descrierea implementarii algoritmului de rezolvare Sudoku
		i)  Etapa secventiala
			Etapa secventiala consta in implementarea unui algoritm de tip 
		Backtracking cronologic care va genera toate solutiile posibile pe 
		baza informatiilor date	la intrarea rularii programului.
			Algoritmul va intra recursiv pana cand sunt toate cele 
		dim_pb * dim_pb casute completate, caz in care va retine solutia in 
		lista de solutii interne partiale procesului.
			In cazul in care, la orice pas, se descopera un esec de verificare 
		a configuratiei	curente(fie in cadrul patratutului curent, fie pe 
		linie/coloana), atunci se va iesi un pas din recursiune.
			Evident, casutele deja completate din input-ul problemei vor fi 
		sarite.
		ii) Comunicarea intre procese
			Fiecare proces isi va trimite lista de solutii partiale doar
		dupa ce acestea au fost realizate toate combinatiile posibile cu toti
		copiii de pe ramura respectiva. Am ales sa fac acest lucru, deoarece
		am vrut sa minimizez numarul de combinatii pe care trebuie sa le faca
		procesele superioare in ierarhie(indeosebi procesul ROOT) - fapt ce 
		va conduce la o folosire mai redusa a memoriei.
			Fiecare proces va astepta un numar de mesaje de tip SUDOKU_DONE,
		numar egal cu numarul de copii directi pe care ii are fiecare proces,
		iar apoi va trimite lista de rezultate partiale interne(dupa combinatii,
		o lista care nu va contine solutiile care nu s-au putut combina cu 
		solutiile tuturor ramurilor) catre parintele procesului. 
			Toate procesele care nu sunt ROOT, in acest moment, se vor incehia.
			In final, procesul ROOT va afisa prima solutie gasita pentru jocul
		de SUDOKU, va primi toate celelate mesaje dar nu va mai incerca sa 
		realizeze combinari(avand deja o solutie) si va termina executia.
			Astfel, toate mesajele trimise vor fi si receptionate, iar prima
		solutie gasita va fi afisata in fisierul de iesire.
			De notat, este faptul ca trimiterea matricei cu casutele libere
		a fost realizata conform enuntului, insa, am considerat util ca procesul
		parinte sa notifice copii si in legatura cu pozitia de inceput a 
		subproblemei de care vor fi responsabili(linia si coloana casutei 
		din stanga sus a subpatratului). Am considerat, ca parcurgerea matricei
		pentru a determina patratul propriu poate fi indepartata in acest fel,
		ducand la o optimizare minora.

	5. Probleme intampinate si solutii gasite
		Principala problema intampinata a fost cresterea eficientei si 
	imbunatatirea timpului de executie a programului.
		In acest sens, am incercat pentru bucatile de cod secventiale sa 
	optimizez numarul de iteratii ale buclelor, spre exemplu pentru a combina 
	doua solutii pentru 2 subprobleme combinarea - si verificarea daca aceasta 
	combinare este corecta - sunt facute simultan, impreuna cu verificarea 
	daca combinarea lor	rezolva intregul joc Sudoku.
		De asemenea, o alta problema de optimizare a constat in ordinea in 
	care sunt trimise solutiile catre parinti. In acest sens, am ales ca 
	fiecare proces sa isi calculeze solutiile partiale proprii si sa le 
	astepte pe cele ale copiilor pentru combinare inainte sa trimita intreaga 
	lista, o singura data, catre parintele sau. Am ales sa fac acest lucru, 
	deoarece trimiterea unor solutii necombinate cu toti copii nodului 
	respectiv va conduce in retinerea unor solutii inutile de catre parintii 
	nodului, deoarece acestea nu vor putea conduce niciodata la o solutie a 
	jocului - ele fiind incomplete(din cauza faptului ca nu s-au realizat 
	toate combinarile cu solutiile de pe ramura respectiva).

	6. Compilare
	Arhiva include un fisier Makefile care compileaza fisierele sursa.

	Obs. De notat, este ca fisierele de tip topologie trebuie sa respecte un 
	format specific referitor la ultima linie: ele se vor termina intr-un 
	singur caracter	terminator de linie "\n".
	     De asemenea, fisierele sudoku.out se termina cu un caracter "\n", iar 
	fiecare linie este urmata de un spatiu dupa ultima cifra afisata.

	     Am testat pe cluster pentru 4/9/16 procesoare toate fisierele de test 
	primite cu cateva configuratii de topologii si am observat corectitudinea
	rezultatelor impreuna cu terminarea tuturor proceselor. Coada folosita
	a fost ibm-opteron4.q.
	
