/*
 *	Botarleanu Robert-Mihai 331CB.
 *  Source file for the topolgy algorithm.
 */

#include "topology.h"

// updates the current topology matrix with the one received from the child
void update_topology_matrix(int source) {
	int i, j;
	for(i = 0; i < N; ++i) {
		char no_data = true;	// check for updates
		for(j = 0; j < N; ++j) 
			if(buf[i * N + j] != 0) { no_data = false; break; }
		if(no_data) continue;	// no updates
		// update the topology matrix and the routing table
		if(i != rank) {
			memcpy(A + i * N, buf + i * N, N);
			routing_table[i] = source;	// use child as next hop
		}
	}
}

// initial diffusal of requests through the topology
void establish_parents(void) {
	int 			i;
	
	if(rank == ROOT) { // root sends the first request
		for( i = 0; i < adj_size; ++i )
			MPI_Send(NULL, 0, MPI_UNSIGNED_CHAR, adj[i], 
				REQUEST, MPI_COMM_WORLD);
		return;
	} 
	// other processes await a request from their parents
	MPI_Recv(buf, 0, MPI_UNSIGNED_CHAR, MPI_ANY_SOURCE, 
			REQUEST, MPI_COMM_WORLD, &status);
	parent = adj[i];
	
	for(i = 0; i < adj_size; ++i) { // diffuse the request to the children
		if(adj[i] != parent) 
			MPI_Send(NULL, 0, MPI_UNSIGNED_CHAR, adj[i], 
				REQUEST, MPI_COMM_WORLD);
	}
}

// starting from the leaf nodes, the adjacency lists are propagated towards
// the root, constructing the topology
void diffuse_adjacencies(void) {
	int nr_e = 0; 
	int max_echos = (rank==0)?(adj_size):(adj_size-1);
	// block requests from children
	while(nr_e < max_echos) {
		MPI_Recv(buf, N * N , MPI_UNSIGNED_CHAR, MPI_ANY_SOURCE, 
			MPI_ANY_TAG, MPI_COMM_WORLD, &status);
		int source = status.MPI_SOURCE;
		int tag = status.MPI_TAG;
			if(tag == ECHO) {
			routing_table[source] = source;		// child, routes directly
			update_topology_matrix(source);
			++nr_e;
		}

		if(tag == ECHO_FALSE) ++nr_e;
		if(tag == REQUEST) { // false echo
			MPI_Send(NULL, 0, MPI_UNSIGNED_CHAR, source, 
				ECHO_FALSE, MPI_COMM_WORLD);
			false_parents[source] = 1;
		}
	}
	if(rank != ROOT) { // diffuse upwards in the hirerachy
		MPI_Send(A, N * N, MPI_UNSIGNED_CHAR, parent, 
			ECHO, MPI_COMM_WORLD);
	}
}

// the root process now diffuses the complete topology to the other processes
void diffuse_topology(void) {
	int 			i;
	if(rank == ROOT) {
		// root sends a TOP notification downwards in the graph
		for(i = 0; i < adj_size; ++i)
			MPI_Send(A, N * N, MPI_UNSIGNED_CHAR, adj[i], 
				TOP, MPI_COMM_WORLD);
		return;
	} 
	// descendants receive the topolgy and diffuse it further
	MPI_Recv(A, N * N , MPI_UNSIGNED_CHAR, parent, 
		TOP, MPI_COMM_WORLD, &status);
	for(i = 0; i < adj_size; ++i) {
		if(adj[i] != parent && !false_parents[adj[i]])
			MPI_Send(A, N * N, MPI_UNSIGNED_CHAR, adj[i], 
					TOP, MPI_COMM_WORLD);
	}
}

void print_to_log_files(void) {
	fprintf(LOG_FILE, "%d\n", rank);
	// print the required information to the process-specific log file
	if(rank == ROOT) { // the root node prints the complete topology
		print_mat(A, N, N);
	}
	// all processes print their routing tables
	print_vec(routing_table, N);
	printf("Tabela de rutare rank %d: ", rank);
	int i;
	for(i = 0; i < N; ++i) printf("%d ", routing_table[i]);
	printf("\n");
}

void complete_routing_table(void) {
	int i = 0;
	for(; i < N; ++i) // for all unknown next_hops, send via parent
		if(routing_table[i] == -1 && i != rank) routing_table[i] = parent;
}

void construct_topology(void) {
	establish_parents();
	diffuse_adjacencies();
	diffuse_topology();	
	complete_routing_table();
	// print to log
	print_to_log_files();
}