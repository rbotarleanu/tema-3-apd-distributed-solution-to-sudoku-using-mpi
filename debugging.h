/*
 *   Botarleanu Robert-Mihai
 *	 Basic debugging and I/O functions.
 */

#ifndef DEBUGGING_HEADER_GUARD
#define DEBUGGING_HEADER_GUARD

#include "stdio.h"
#include "main.h"
// print a vector
void print_vec(int *v, int size);
// print a matrix
void print_mat(unsigned char *A, int height, int width);
// convert a topology algorithm tag to a string
const char* conv_tag(int tag);

#endif