/*
 *	Botarleanu Robert-Mihai 331CB.
 *  Source file for the topolgy algorithm.
 */
#include "sudoku.h"

void sudoku_create_problem(int pb_size, int row, int col, int *v) {
	// alocate one problem
	p = (prb*) calloc(1, sizeof(prb));
	p->ps = pb_size;			// the dim_pb size
	p->world_row = row;			// row in global problem
	p->world_col = col;			// column in global problem
	p->solutions = NULL;		// internal solutions
	p->internal_solutions = NULL;	// combined solutions
	// alocate the problema rray
	p->problem = (int*) calloc(pb_size * pb_size * pb_size * pb_size,
		sizeof(int));
	int 	i, j;
	int size = pb_size * pb_size;
	// copy over the array
	for(i = 0; i < size; ++i)
		for(j = 0; j < size; ++j)
			p->problem[i * size + j] = v[i * size + j];
}

// checks if the backtracking step is correct
int check(int *v, int pos, int n, int row, int col) {
	int i; 
	// inside square & row column
	for(i = 0; i < n; ++i) {
		int r_i = p->world_row + (i / p->ps);
		int r_j = p->world_col + (i % p->ps);
		if(v[r_i * n + r_j] == v[row * n + col]  
			&& i != pos) return 0;	// inside square
		if(v[row * n + i] == v[row * n + col] &&	
			col != i) return 0;		// column
		if(v[i * n + col] == v[row * n + col] &&
			row != i) return 0;		// row
	}	
	return 1;	// curent solution is good
}

// using backtracking, compute all possible solutions 
// for subproblem
void sudoku_baktrack(int *v, int n, int step) {
	if(step == n) {	// have a full solution, save it
		list_add(&(p->solutions), 0, v, n * n);
		return;
	}
	// row & column in global coordinates
	int row = p->world_row + (step / p->ps);
	int col = p->world_col + (step % p->ps);
	
	if(v[row * n + col] != 0) { // there is already something here
		sudoku_baktrack(v, n, step+1); 	// go to next step
		return;
	}
	int i;
	for(i = 1; i <= n; ++i) {	// try each possible combination 
		v[row * n + col] = i;	// for current step
		if(check(v, step, n, row, col))  // plausible solution
			sudoku_baktrack(v, n, step + 1);
		v[row * n + col] = 0;	// try another
	}
}

void sudoku_generate_partial_solutions() {
	if(!p) return;
	int 		i, j;
	// copy the problem array in a temporary buffer
	int *temp = (int*) calloc(p->ps * p->ps * p->ps * p->ps, sizeof(int));
	int len = p->ps * p->ps;
	for(i = 0; i < len; ++i)
		for(j = 0; j < len; ++j)
			temp[i * len + j] = p->problem[i * len + j];
	sudoku_baktrack(temp, len, 0);	// find all solutions with backtracking
	free(temp);	// free the buffer
}

int sudoku_combine_two_solutions(int *com, int *sol1, int *sol2, int ps, int *full) {
	int i = 0, j = 0;
	// merge the two solutions
	int dim = ps * ps * ps * ps;
	int rlen = ps * ps;
	int count = 0;
	// clear the combined buffer
	memset(com, 0, sizeof(int) * dim);
	for(i = 0; i < rlen; ++i) {
		for(j = 0; j < rlen; ++j) {
			// if there is a value here, copy it to the result buffer
			if(sol1[i * rlen +j] > 0) // in first solution
				{ com[i * rlen +j] = sol1[i * rlen + j]; }
			if(sol2[i * rlen +j] > 0) // in second solution
				{ com[i * rlen +j] = sol2[i * rlen + j]; }
			if(com[i * rlen +j] > 0)  // this cell is filled
				++count;
			else continue;
			int k = 0;	// check row-column 
			for(k = 0; k < rlen; ++k) {
				if(k != i && (com[i * rlen +j] == com[k * rlen + j])) {
					return 0; // 2 values on same row
				}
				if(k != j && (com[i * rlen +j] == com[i * rlen + k])) {
					return 0; // 2 values on same column
				}
			}
		}
	}
	// are all cells filled
	*full = (count == dim) ? 1 : 0;
	return 1;	// success
}

void sudoku_combine_partial_solutions(int *other, int other_size, int pb_size) {
	int o_index = 0;
	int dim = pb_size * pb_size * pb_size * pb_size;
	int len = pb_size * pb_size;
	int *aux = (int*) calloc(dim, sizeof(int));
	int *rez = (int*) calloc(dim, sizeof(int));
	list sol_ptr;	
	// go through each solution from other process
	for(; o_index < other_size; ++o_index) {
		// copy it to an auxiliary buffer
		memcpy(aux, other + (o_index * dim), 
			dim * sizeof(int));
		// go through each local partial solution
		for(sol_ptr = p->solutions; sol_ptr != NULL; sol_ptr = sol_ptr->next) {
			int full = 0;	// to check if the solution is complete
			if(sudoku_combine_two_solutions(rez, sol_ptr->a, 
					aux, pb_size, &full)) {	// combine 
				// save if the combination is good
				list_add(&(p->internal_solutions), 0, rez, dim);
				if(rank != 0) {
				} else if(full) {
					// this is the first problem solution
					// print it
					int i, j;
					outf = fopen(OUT, "wt"); // open output file
					fprintf(outf, "%d\n", p->ps);
					for(i = 0; i < len; ++i){
						for(j = 0; j < len; ++j) {
							fprintf(outf, "%d ", rez[i * len + j]);
						}
						fprintf(outf, "\n");
					}
					// we have a solution, no point in doing any more
					// computations
					solution_found = true;
					return;
				}
				// reset the result
				memset(rez, 0, sizeof(int) * dim);
			}
		}
	}
	// clear the local partial solutions and change it to be the 
	// successful combinations
	list_destroy(&p->solutions);
	p->solutions = p->internal_solutions;
	p->internal_solutions = NULL;
	// free buffers
	free(aux);
	free(rez);
}

void sudoku_send_all_to_parent() {
	int *v, len;
	// compress solutions and send to parent
	v = compress_to_array(p->solutions, &len);
	MPI_Send(v, len, MPI_INT, parent, UPDATE, MPI_COMM_WORLD);
	free(v);
}

void sudoku_destroy_problem() {
	if(!p) return;
	// destroy lists and dealocate
	list_destroy(&(p->solutions));
	list_destroy(&(p->internal_solutions));
	free(p->problem);
	free(p);
}
