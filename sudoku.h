/*
 *  Botarleanu Robert-Mihai 331CB
 *  Solves a sudoku subproblem and combines solutions
 *  for multiple sub-squares.
 */

#ifndef SUDOKU_HEADER_GUARD
#define SUDOKU_HEADER_GUARD

#include "main.h"

// sudoku problem type
typedef struct sudoku_problem {
	int ps;				// the size of the problem
	int	world_row, world_col;		// coordinates of the local problem
	int *problem;					// liniarized submatrix

	struct integer_array_list* solutions;
	struct integer_array_list* internal_solutions;
}prb;

// current process subproblem
prb *p;

// load a problem
void sudoku_create_problem(int pb_size, int row, int col, int *v);
// generate the partial solutions for dim_pb * dim_pb square
void sudoku_generate_partial_solutions();
// combine local solutions with those gotten from another process
void sudoku_combine_partial_solutions(int *other, int other_size, int pb_size);
// dealocate all memory
void sudoku_destroy_problem();
// send internal solutions to the parent
void sudoku_send_all_to_parent();

#endif