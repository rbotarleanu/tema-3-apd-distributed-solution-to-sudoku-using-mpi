/*
 * 	Botarleanu Robert-Mihai 331CB
 *  A simple linked list implementation where the data is a dynamically 
 *  alocated array.
 */

#ifndef LIST_HEADER_GUARD
#define LIST_HEADER_GUARD

#include <string.h>
#include <stdlib.h>
#include "mpi.h"
#include "debugging.h"

typedef struct integer_array_list {
	int *a;					// data - array
	int size;				// array size
	struct integer_array_list *next;	// next pointer
}node, *list;

/* basic list operations */
// add element to position
void list_add(list *l, int pos, int *el, int size);
// remove element from position
list list_remove(list *l, int pos);
// print the list to LOG_FILE
void list_print(list l);
// dealocate memory
void list_destroy(list *l);
// find the size of the list
int list_size(list l);

/* functions used to help communicate between proccesses */
// compresses the list to a contigous memory block
int* compress_to_array(list l, int *len);
// splits a continous array into a list
list decompress_from_array(int* v, int len, int tsize);

#endif