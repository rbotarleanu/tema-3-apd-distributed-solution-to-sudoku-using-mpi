/* Program entry point. 
 * Contains the actual algorithm for solving the sudoku problem
 * described in the assignment.
 */

#include "main.h"

// compute exponential in logarithmic time
int logpow(int base, int exponent) {
	if( exponent == 0 ) return 1;
	if( exponent == 1 ) return base;
	int half = logpow(base, exponent/2);
	if(exponent %2 == 1) return half * base * half;
	return half * half;
}

// load file names to variables
void load_file_names(char **argv) {
	memcpy(TOPOLOGY_FILE, argv[1], MAX_FILE_NAME_LEN);
	memcpy(IN, argv[2], MAX_FILE_NAME_LEN);
	memcpy(OUT, argv[3], MAX_FILE_NAME_LEN);
}

// reads the adjacency list for the current process from file
int read_adjacency_list(int rank) {
	FILE *fin = fopen(TOPOLOGY_FILE, "r");
	if(fin == NULL) return 1;

	adj_size = 0;

	ssize_t 	read_size;
	char		line[MAX_LINE_SIZE];
	while(fgets(line, MAX_LINE_SIZE, fin) != NULL) {
		line[strlen(line) - 1] = '\0';	// strip \n
		int 	id;
		char	adjacency[MAX_LINE_SIZE];

		sscanf(line, "%d -", &id);
		if(id != rank) continue;	// not the adjacency for this process
		// start reading the adjacency list
		char *adj_start;
		adj_start = strchr(line, '-');	// search for the - sign
		char *toks;
		toks = strtok(adj_start + 1, " ");	// split to find the numbers
		while(toks != NULL) {	// get the adjbours
			sscanf(toks, "%d", &adj[adj_size++]);
			toks = strtok(NULL, " ");
		}
		break; 	// quit reading
	}
	fclose(fin);
	return 0;
}

// reads the input(the root does this)
void read_input() {
	int 			i, j;
	FILE *sudoku_in = fopen(IN, "rt");
	fscanf(sudoku_in, "%d", &pb_size);
	// allocate the problem 
	sudoku = (int *) calloc(logpow(pb_size * pb_size, 2), sizeof(int));
	int len = pb_size * pb_size; 	// row/column length
	for(i = 0; i < len; ++i) // read each element
		for(j = 0; j < len; ++j) fscanf(sudoku_in, "%d", &sudoku[i * len + j]);
	fclose(sudoku_in);
}

// unmarks all marked values in a range
void unmark(int *a, int len, int x, int y) {
	int 		i, j;
	int 		size = logpow(len, 2);
	for(i = x; i < x + len; ++i)
		for(j = y; j < y + len; ++j)
			if(a[i * size + j] == -1) a[i * size + j] = 0;
}

// marks all values
void mark_all(int *a, int len) {
	int 		i, j;
	int 		size = logpow(len, 4);
	for(i = 0; i < size; ++i)
		if(!a[i]) a[i] = -1;
}

// calculates the branch size of the direct child
int get_branch_size(int direct_descendant) {
	int   		i, cnt = 0;
	for(i = 0; i < N; ++i)	// count child apparitions in routing table
		if(routing_table[i] == direct_descendant) ++cnt;
	return cnt;
}

// splits the poblem to children
void split_to_children(int *a, int pb_size, int x, int y) {
	int   		i, j;
	int 		sx = x, sy = y;
	int 		buf[3];
	int 		array_size = logpow(pb_size * pb_size, 2);

	// generate a table of all possible subproblems
	int 		*px, *py, *marked;
	px = (int*) calloc(pb_size * pb_size, sizeof(int));
	py = (int*) calloc(pb_size * pb_size, sizeof(int));
	int 		psz = 0;

	i = j = 0;
	while(i < pb_size * pb_size) {
		px[psz] = i, py[psz++] = j;
		j += pb_size;
		if(j > pb_size * pb_size - pb_size) i += pb_size, j = 0;
	}

	// skip the subproblem of the current process
	sy += pb_size;
	if(sy > pb_size * pb_size - pb_size) sx += pb_size, sy = 0;
	
	// start splitting for children from (x,y)
	for(i = 0; i < adj_size; ++i) {
		int w = adj[i];
		if(false_parents[w] || w == parent) continue;
		int cnt = get_branch_size(w);
		buf[0] = pb_size, buf[1] = sx, buf[2] = sy;
		// send problem data
		MPI_Send(buf, 3, MPI_INT, w, PROBLEM, MPI_COMM_WORLD);
		
		// mark all other subproblems
		mark_all(a, pb_size);
		// unmark for this branch
		for(j = 0; j < cnt; ++j) {
			unmark(a, pb_size, sx, sy);
			// go to next subproblem
			sy += pb_size;
			if(sy > pb_size * pb_size - pb_size) sx += pb_size, sy = 0;
		}

		// send the array
		MPI_Send(a, array_size, MPI_INT, w, PROBLEM, MPI_COMM_WORLD);
	}
	free(px);
	free(py);
}

// diffuses the subproblems to the children
void diffuse_subproblems() {
	int 			i, j;
	int array_size;
	int *diffusal_array;

	if(rank == ROOT) { // rank 0 sends the first messages
		int array_size = logpow(pb_size * pb_size, 2);
		int *diffusal_array = (int *) calloc(array_size, sizeof(int));
		memcpy(diffusal_array, sudoku, sizeof(int) * array_size);
		
		// makes his problem and marks it off the array
		sudoku_create_problem(pb_size, 0, 0, diffusal_array);
		split_to_children(diffusal_array, pb_size, 0, 0);
	} else { 
		int probl[3];	
		// receive the problem size and starting (x,y) coordinates for the 
		// child process
		MPI_Recv(probl, 3 , MPI_INT, parent, PROBLEM, MPI_COMM_WORLD, &status);	
		pb_size = probl[0];						// problem size
		int sx = probl[1], sy = probl[2];		// starting coords
		// allocate the problem array
		array_size = logpow(pb_size * pb_size, 2);
		diffusal_array = (int *) calloc(array_size, sizeof(int));
		// receive the array
		MPI_Recv(diffusal_array, array_size , MPI_INT, 
			parent, PROBLEM, MPI_COMM_WORLD, &status);	
		// create the problem
		sudoku_create_problem(pb_size, sx, sy, diffusal_array);
		// continue splitting to branch
		split_to_children(diffusal_array, pb_size, sx, sy);
	}
}

// solves the sudoku problem
void solve_sudoku() {
	int 		i;
	int  		*recv_buf;
	
	sudoku_generate_partial_solutions();
	// all but leafs receive solutions
	recv_buf = (int *) calloc(MAX_BUFFER_SIZE, sizeof(int));

	int nr_done = 0;
	// count how many DONE messages this process should receive
	// from it's children
	for(i = 0; i < adj_size; ++i) {
		if( adj[i] != parent && !false_parents[adj[i]] ) {
			++nr_done;
		}
	}

	while(nr_done > 0) {	// branch is still active
		// receive solutions
		MPI_Recv(recv_buf, MAX_BUFFER_SIZE , MPI_INT, MPI_ANY_SOURCE, 
			MPI_ANY_TAG, MPI_COMM_WORLD, &status);
		int source = status.MPI_SOURCE;
		int tag = status.MPI_TAG;
		// child branch is done
		if(tag == SUDOKU_DONE) { --nr_done; continue;  }
		if(solution_found) continue;	// consume the rest of the messages
		// find how many solutions we received	
		int count = 0;
		MPI_Get_count(&status, MPI_INT, &count);
		int no_solutions = count / (logpow(pb_size * pb_size, 2));
		// combine with local solutions
		sudoku_combine_partial_solutions(recv_buf, no_solutions, pb_size);
	}

	// processes are done here, send solutions to parents and close
	if(rank != 0) sudoku_send_all_to_parent();
	sudoku_destroy_problem();	// empty problem
	if(rank != 0) {	// notify parent of closure
		MPI_Send(NULL, 0, MPI_INT, parent, SUDOKU_DONE, MPI_COMM_WORLD);
	}
	free(recv_buf);	// dealocate receival buffer
}

// entry point
int main(int argc, char **argv) {
	int 		i;
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &N);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	// check command line arguments
	if(argc != 4) {
		if(rank == ROOT) {
			printf("Invalid parameters. Usage:\n"
					"./binary <topology_file> <input_sudoku_file> "
					"<output_sudoku_file>\n"
				);
		} // exit on usage failure
		MPI_Finalize();
		return 0;
	}

	// allocate the adjacency and buffer liniarized matrices
	A = (byte*) calloc(N * N, sizeof(byte));
	buf = (byte*) calloc(N * N, sizeof(byte));
	
	// allocate the routing table
	routing_table = (int*) calloc(N, sizeof(int));
	memset(routing_table, -1, N * sizeof(int));

	// read the adjacency list
	load_file_names(argv);
	if(read_adjacency_list(rank)) {
		printf("Process %d was unable to read topology file \"%s\".\n",
			rank, TOPOLOGY_FILE);
		MPI_Finalize();
		return 1;
	}	
	
	// put the coresponding line in the topology matrix
	for(i = 0; i < adj_size; ++i) A[rank * N + adj[i]] = 1;

	// prepare the log file
	char fname[MAX_FILE_NAME_LEN];
	sprintf(fname, "logs/log%d", rank);
	LOG_FILE = fopen(fname, "wt");

	// run the algorithm to construct the topology
	construct_topology();
	printf("Topology done rank %d\n", rank);
	// rank 0 reads the input file
	if(rank == 0) {
		read_input();			 // read the problem
		// allocate the solutions array
		max_solutions = logpow(pb_size * pb_size, pb_size * pb_size);
		if(max_solutions <= 0) max_solutions = DEFAULT_MAX_SOLUTIONS;
	}
	// diffuse problems to branches
	diffuse_subproblems();
	// solve the sudoku and print to file
	solve_sudoku();

	// deallocate memory and close the log file
	free(A);
	free(buf);
	fclose(LOG_FILE);
	free(routing_table);

	// rank 0 dealocation
	if(rank == 0) free(sudoku);
	printf("PRocess %d done\n", rank);
	MPI_Finalize();
	return 0;
}
